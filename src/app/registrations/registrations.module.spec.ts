import {TestBed, async, inject} from '@angular/core/testing';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {RegistrationsModule} from './registrations.module';
import { RouterTestingModule } from '@angular/router/testing';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {MostModule} from '@themost/angular';
// important: exclude test because of a karma error on linux
// todo: fix error on linux
describe('RegistrationsModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientTestingModule,
        TranslateModule.forRoot(),
        MostModule.forRoot({
          base: '/',
          options: {
            useMediaTypeExtensions: false
          }
        })
      ]
    });
  }));
  it('should create an instance', inject([TranslateService], (translateService: TranslateService) => {
    const registrationModule = new RegistrationsModule(translateService);
    expect(registrationModule).toBeTruthy();
  }));
});
