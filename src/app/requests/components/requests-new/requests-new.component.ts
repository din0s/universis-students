import {Component, OnInit, ViewChild, TemplateRef, OnDestroy} from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { Router, ActivatedRoute } from '@angular/router';
import {ConfigurationService, ErrorService} from '@universis/common';
import { ApplicationSettings } from '../../../students-shared/students-shared.module';
import { ModalService } from '@universis/common';
import { TranslateService } from '@ngx-translate/core';
import {Subscription} from 'rxjs';
import {RequestsService} from '../../services/requests.service';
import {ResponseError} from '@themost/client';

@Component({
  selector: 'app-requests-new',
  templateUrl: './requests-new.component.html',
  styleUrls: ['./requests-new.component.scss']
})
export class RequestsNewComponent implements OnInit, OnDestroy {
  @ViewChild('templateFail') failTemplate: TemplateRef<any>;

  public requestHeader;
  public isLoading = false;
  public hasError = false;
  private modalRef: any;

  private querySubscription: Subscription;

  constructor(
    private route: ActivatedRoute,
    private _router: Router,
    private _context: AngularDataContext,
    private modalService: ModalService,
    private _translateService: TranslateService,
    private _configurationService: ConfigurationService,
    private _activatedRoute: ActivatedRoute,
    private _requestService: RequestsService,
    private _errorService: ErrorService) {

  }

  public model = {
    name: null,
    alternateName: null,
    description: null,
    object: {
      alternateName: null
    }
  };

  ngOnInit() {
    // get header request message from app.json
    const messages = this._configurationService.settings.app && (<ApplicationSettings>this._configurationService.settings.app).messages;
    if (Array.isArray(messages)) {
      const message = messages.find(x => {
        return x.additionalType === 'StudentRequestHeader' && x.inLanguage === this._configurationService.currentLocale;
      });
      if (message) {
        this.requestHeader = message.body;
      }
    }
    this.querySubscription = this._activatedRoute.queryParams.subscribe((queryParams) => {
      if (queryParams.type) {
        // get request type
        this._requestService.getRequestTypes().then( (requestTypes) => {
          // find request
          const findRequestType = requestTypes.find( (requestType) => {
            return requestType.alternateName === queryParams.type;
          });
          if (findRequestType == null) {
            return this._errorService.showError(new ResponseError('The specified request type cannot be found.', 404), {
              continueLink: '/requests/list'
            });
          }
          Object.assign(this.model, {
            name: findRequestType.name,
            alternateName: findRequestType.alternateName,
            object: {
              alternateName: findRequestType.alternateName
            }
          });
        });
      } else {
        // tslint:disable-next-line:max-line-length
        return this._errorService.showError(new ResponseError('You have reached this page with invalid parameters. Request type cannot be empty', 400), {
          continueLink: '/requests/list'
        });
      }
    });
  }

  async submit() {
    this.isLoading = true;
    this.hasError = false;

    const action = (this.model.alternateName === 'OtherRequest') ? 'RequestMessageActions' : 'RequestDocumentActions';
    if (action === 'RequestMessageActions') {
      this.model.object = null;
    }

    try {
      await this._context.model(action).save(this.model);
      return this._router.navigate(['/requests/list']);
    } catch (err) {
      this.hasError = true;
      this.isLoading = false;
      this.modalRef = this.modalService.openModal(this.failTemplate, 'modal-lg');
    }
  }

  closeModal(): void {
    this.modalRef.hide();
  }

  ngOnDestroy(): void {
    if (this.querySubscription) {
      this.querySubscription.unsubscribe();
    }
  }
}
