import {CUSTOM_ELEMENTS_SCHEMA, ModuleWithProviders, NgModule, OnInit, Optional, SkipSelf} from '@angular/core';
import { CommonModule } from '@angular/common';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {environment} from '../../environments/environment';
import {ApplicationSettingsConfiguration, SharedModule} from '@universis/common';
import { FormsModule } from '@angular/forms';
import { BsDropdownModule, ModalModule } from 'ngx-bootstrap';
import { NgSpinKitModule } from 'ng-spin-kit';
import { RouterModule } from '@angular/router';
import { LangComponent } from './lang-component';
import {AppSidebarService} from './services/app-sidebar.service';
import {TooltipModule} from 'ngx-bootstrap';


export declare interface ApplicationSettings extends ApplicationSettingsConfiguration {
  useDigitalSignature: boolean;
  navigationLinks?: INavigationLinkConfiguration;
  messages?: any;
  title?: any;
  header?: Array<any>;
}

export declare interface INavigationLinkConfiguration {
  name: string;
  url: string;
  inLanguage: string;
  target: "_self" | "_blank" | "_parent" | "_top" | string;
}

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    SharedModule,
    FormsModule,
    BsDropdownModule,
    ModalModule,
    NgSpinKitModule,
    TooltipModule,
    RouterModule
  ],
  declarations: [
    LangComponent
  ],
  exports: [
    LangComponent
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class StudentsSharedModule implements OnInit {
  constructor(@Optional() @SkipSelf() parentModule: StudentsSharedModule,
              private _translateService: TranslateService) {
    if (parentModule) {
      // throw new Error(
      //    'StudentsSharedModule is already loaded. Import it in the AppModule only');
    }
    this.ngOnInit().catch(err => {
      console.error('An error occurred while loading students shared module');
      console.error(err);
    });
  }

  static forRoot(): ModuleWithProviders {
    return {
      ngModule: StudentsSharedModule,
      providers: [
        AppSidebarService
      ]
    };
  }

  async ngOnInit() {
    // create promises chain
    const sources = environment.languages.map(async (language) => {
      const translations = await import(`../../assets/i18n/${language}.json`);
      this._translateService.setTranslation(language, translations, true);
    });
    // execute chain
    await Promise.all(sources);
  }

}
