import {Component, OnInit} from '@angular/core';
import { UserService } from '@universis/common';
import { ProfileService } from '../../services/profile.service';
import { LoadingService } from '@universis/common';


@Component({
    selector: 'app-profile-preview',
    templateUrl: 'profile-preview.component.html',
    styleUrls: ['profile-preview.component.scss']
})

export class ProfilePreviewComponent implements OnInit {

    // student object coming from the API
    public student: any;
    public loading = true;   // Only if data is loaded

    constructor(private _userService: UserService,
        private _profileService: ProfileService,
        private loadingService: LoadingService) {
    }

    ngOnInit() {
        this.loadingService.showLoading();  // show loading
        this._profileService.getStudent().then(res => {
            this.student = res; // Load data
            this.loading = false; // Data is loaded
            this.loadingService.hideLoading(); // hide loading
        });
    }
}
