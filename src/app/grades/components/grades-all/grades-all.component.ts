import { Component, OnInit } from '@angular/core';
import {GradesService, CourseGradeBase, GradeAverageResult} from '../../services/grades.service';
import {AngularDataContext} from '@themost/angular';
import {GradeScale} from '@universis/common';
import { LoadingService } from '@universis/common';
import { ChartsModule} from 'ng2-charts';
import {FilterByPipe} from 'ngx-pipes';

@Component({
  selector: 'app-grades-all',
  templateUrl: './grades-all.component.html',
  styleUrls: ['./grades-all.component.scss']
})
export class GradesAllComponent implements OnInit {

  public semesters: number[] = [];
    /**
     * Gets or sets the grades component configuration
     */
  public configuration: any;
    /**
     * Gets or sets the group data based on the selected group configuration
     */
  public groups: any;
    /**
     * Gets or sets a the collection of student course grades
     */
  public data: any = [];
    /**
     * Gets or sets the selected group configuration
     */
  public selectedGroup: any;

  public defaultGradeScale: GradeScale;

  public isLoading = true;

  /* for stats box */
  public weightedAverageObj: GradeAverageResult = {
    courses: 0,
    coefficients: 0,
    average: 0,
    ects: 0,
    units: 0,
    passed: 0,
    grades: 0,
  };
  public simpleAverageObj: GradeAverageResult = this.weightedAverageObj;
  public passedGradeSimpleAverage = '0';
  public passedGradeWeightedAverage = '0';
  public registeredCourses: number;
  public passedCourses: number;
  public failedCourses: number;
  public totalEcts: number;
  public showPassed = false;

  constructor(private gradesService: GradesService,
              private loadingService: LoadingService,
              private contextService: AngularDataContext,
              private filterPipe: FilterByPipe) {
  }

  ngOnInit() {
    // show loading
    this.loadingService.showLoading();
    // load configuration
      import('../../config/grades.config.json').then(gradesConfiguration => {
          // set configuration
          this.configuration = gradesConfiguration.default ? gradesConfiguration.default : gradesConfiguration;
          // set selected group
          this.selectedGroup = this.configuration && this.configuration.groups && this.configuration.groups[0];
          // get grades
          return this.gradesService.getGradeInfo()
              .then((res) => {
                  // get default grade scale
                  return this.gradesService.getDefaultGradeScale().then(gradeScale => {
                      // set grade scale
                      this.defaultGradeScale = gradeScale;
                      // set grades
                      this.data = res;
                      // keep all student courses to studentCourses variable before filtering
                      const studentCourses = this.data;
                      // filter data (remove course parts)
                      this.data = this.data.filter(studentCourse => {
                          return studentCourse.courseStructureType && studentCourse.courseStructureType.id !== 8;
                      });
                      // add courseParts as courses to each parent course
                      this.data.forEach( studentCourse => {
                          // get course parts
                          if (studentCourse.courseStructureType && studentCourse.courseStructureType.id === 4) {
                              studentCourse.courses = studentCourses.filter(course => {
                                  return course.parentCourse === studentCourse.course.id;
                              }).sort((a, b) => {
                                  return a.course.displayCode < b.course.displayCode ? -1 : 1;
                              });
                          }
                      });
                      // force group change
                      this.onChangeGroup(this.selectedGroup.attribute);
                      // hide loading
                      this.loadingService.hideLoading();
                      this.isLoading = false;
                  });
              });
        }).catch( err => {
          this.loadingService.hideLoading();
          throw err;
      });
  }

  /**
     *
     * @param {string} groupByAttribute
     */
  getGroupData(groupByAttribute) {
      return this.contextService.model('students/me/courses')
          .select(groupByAttribute, `count(${groupByAttribute}) as count`)
          .groupBy(groupByAttribute)
          .orderBy(groupByAttribute)
          .getItems().then( groups => {
                return groups;
          });
  }

  onSearchKeyDown($event: KeyboardEvent) {
      if ($event.keyCode === 13) {
          $event.preventDefault();
          // get search text
          const searchText = (<HTMLInputElement>$event.target).value;
          if (searchText.length === 0) {
              // apply grouping
              return this.applyGrouping(this.data, this.selectedGroup.attribute);
          }
          // build regular expression
          const searchRegExp = new RegExp(searchText, 'ig');
          const filtered = this.data.slice(0).filter( x => {
                return searchRegExp.test(x.course.name) || searchRegExp.test(x.course.displayCode);
          });
          // apply grouping
          this.applyGrouping(filtered, this.selectedGroup.attribute);
      }
  }

  applyGrouping(data: any, attribute: string) {

      this.getGroupData(attribute).then(result => {
          // get default grade scale
          const gradeScale = this.defaultGradeScale;
          // group grades (by filtering only passed courses)
          result.forEach( group => {
              group.grades = data.filter( x => {
                  if (group) {
                      return x[attribute] && (x[attribute].id === group[attribute].id);
                  }
              });

              const gradeGroup: Array<CourseGradeBase> = [];

              group.grades.forEach(course => {
                const courseGradeBaseObj: CourseGradeBase = {
                  courseStructureType: course.courseStructureType,
                  units: course.units,
                  calculateGrade: course.calculateGrade,
                  calculateUnits: course.calculateUnits,
                  isPassed: course.isPassed,
                  ects: course.ects,
                  coefficient: course.coefficient,
                  grade: course.grade
                };
                gradeGroup.push(courseGradeBaseObj);
              });
            const simpleAverageObject = this.gradesService.getGradesSimpleAverage(gradeGroup);
            const weightedAverageObject = this.gradesService.getGradesWeightedAverage(gradeGroup);

            group.simpleAverage = gradeScale.format(simpleAverageObject.average);
            group.ects = weightedAverageObject.ects;
            group.courses = weightedAverageObject.courses;

          });
          // set groups
          this.groups = result;
          if(this.showPassed) {
            this.groups.forEach(group => {
              group.grades = this.filterPipe.transform(group.grades, ['isPassed'], 1);
            });
          }
          const courseBaseArray: Array<CourseGradeBase> = [];
          this.data.forEach(course => {
            const courseBaseData: CourseGradeBase = {
              courseStructureType: course.courseStructureType,
              coefficient: course.coefficient,
              calculateUnits: course.calculateUnits,
              calculateGrade: course.calculateGrade,
              isPassed: course.isPassed,
              ects: course.ects,
              grade: course.grade,
              units: course.units
            };
            courseBaseArray.push(courseBaseData);
          });

          this.simpleAverageObj = this.gradesService.getGradesSimpleAverage(courseBaseArray);
          this.weightedAverageObj = this.gradesService.getGradesWeightedAverage(courseBaseArray);
          if (this.weightedAverageObj) {
            // set passed courses
            this.passedCourses = this.weightedAverageObj.passed;
            // set failed courses
            this.failedCourses = this.weightedAverageObj.courses - this.weightedAverageObj.passed;
            // set sum of courses
            this.registeredCourses = this.weightedAverageObj.courses;
            // set Weighted average of passed courses
            this.passedGradeWeightedAverage = gradeScale.format(this.weightedAverageObj.average);
            // set total ects
            this.totalEcts = this.weightedAverageObj.ects;
          }
          if (this.simpleAverageObj) {
            // set Simple average of passed courses
            this.passedGradeSimpleAverage = gradeScale.format(this.simpleAverageObj.average);
          }
      });
  }

    /**
     * Handles the event of group attribute change
     * @param {*} selected
     */
  onChangeGroup(selected: any) {
        // get courses groups
        const selectedGroup = this.configuration.groups.find(value => {
               return value.attribute === selected;
        });
        // get group attribute
        const attribute = selectedGroup.attribute;
        // ensure grades
        const data = (this.data || []).sort((a, b) => {
          return a.course.displayCode < b.course.displayCode ? -1 : 1;
        });
        // set selected group
        this.selectedGroup = selectedGroup;
        // apply grouping
        this.applyGrouping(data, this.selectedGroup.attribute);
    }

  filterOnlyPassedCourses(passedCourses: boolean){
    this.showPassed = passedCourses;
    if (this.showPassed) {
      for (const group of this.groups){
        group.grades = this.filterPipe.transform(group.grades, ['isPassed'], 1);
      }
    }
    if(!this.showPassed){
      this.onChangeGroup(this.selectedGroup.attribute);
    }
  }
}
