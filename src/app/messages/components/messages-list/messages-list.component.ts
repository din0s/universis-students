import {Component, OnDestroy, OnInit} from '@angular/core';
import {MessagesService} from '../../services/messages.service';
import {LoadingService} from '@universis/common';
import {MessageSharedService} from '../../../students-shared/services/messages.service';
import {ActivatedRoute, Route, Router} from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-messages-list',
  templateUrl: './messages-list.component.html',
  styleUrls: ['./messages-list.component.scss'],
  providers: [ MessagesService]
})
export class MessagesListComponent implements OnInit, OnDestroy {

  public searchText = '';
  public messages: any;
  public skip = 0;
  public selectedMessages = null;
  public take = 10;
  public bodyLength = 500;
  public count: any;
  public subscription: Subscription;
  public isLoading = false;
  selector = '.inner__content';

  constructor(private notificationsMsgService: MessagesService,
              private loadingService: LoadingService,
              private messageSharedService: MessageSharedService,
              private _activatedRoute: ActivatedRoute,
              private _router: Router,
              private _sanitizer: DomSanitizer) {
  }

  ngOnInit() {
    // get today messages
    this.messages = [];
    this.selectedMessages = 'today';
    this._activatedRoute.params.subscribe( params => {
        this.selectedMessages = params['selectedMessages'];
        this.getMessages(this.selectedMessages, 0, 0);
      }
    );
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  getMessages(selected: any, skip: any, scrollValue: any) {
    this.messages = scrollValue === 0 ? [] : this.messages;
    if (selected !== 'all') {
      this.searchText = '';
    }
    if (this.searchText.length === 0) {
      const currentDate = new Date();
      this.skip = skip;
      this.isLoading = true;
      this.loadingService.showLoading();
      if (selected === 'today') {
        this.notificationsMsgService.getMessagesByDate(this.take, this.skip, currentDate.toISOString().substring(0, 10))
          .then((res) => {
            this.messages = (this.messages || []).concat(res.value);
            this.count = res.total;
            (this.messages || []).map((message) => {
              message.dateReceived == null ? message.read = false : message.read = true;
              message.body = this._sanitizer.sanitize(1, message.body);
              return message;
            });
            // hide loading
            this.loadingService.hideLoading();
          });
      } else if (selected === 'week') {
        currentDate.setDate(currentDate.getDate() - 7);
        this.notificationsMsgService.getMessagesByDate(this.take, this.skip, currentDate.toISOString().substring(0, 10))
          .then((res) => {
            this.messages = (this.messages || []).concat(res.value);
            this.count = res.total;
            (this.messages || []).map((message) => {
              message.dateReceived == null ? message.read = false : message.read = true;
              message.body = this._sanitizer.sanitize(1, message.body);
              return message;
            });
            // hide loading
            this.isLoading = false;
            this.loadingService.hideLoading();
          });
      } else if (selected === 'month') {
        currentDate.setMonth(currentDate.getMonth() - 1);
        this.notificationsMsgService.getMessagesByDate(this.take, this.skip, currentDate.toISOString().substring(0, 10))
          .then((res) => {
            this.messages = (this.messages || []).concat(res.value);
            this.count = res.total;
            (this.messages || []).map((message) => {
              message.dateReceived == null ? message.read = false : message.read = true;
              message.body = this._sanitizer.sanitize(1, message.body);
              return message;
            });
            // hide loading
            this.isLoading = false;
            this.loadingService.hideLoading();
          });
      } else if (selected === 'unread') {
        this.notificationsMsgService.getUnreadMessages(this.take, this.skip)
          .then((res) => {
            this.messages = (this.messages || []).concat(res.value);
            this.count = res.total;
            (this.messages || []).map((message) => {
              message.dateReceived == null ? message.read = false : message.read = true;
              message.body = this._sanitizer.sanitize(1, message.body);
              return message;
            });
            // hide loading
            this.isLoading = false;
            this.loadingService.hideLoading();
          });
      } else {
        this.notificationsMsgService.getAllMessages(this.take, this.skip)
          .then((res) => {
            this.messages = (this.messages || []).concat(res.value);
            this.count = res.total;
            this.messages = this.messages.map((msg) => {
              msg.dateReceived == null ? msg.read = false : msg.read = true;
              return msg;
            });
            // hide loading
            this.isLoading = false;
            this.loadingService.hideLoading();
          });
      }
    } else {
      this.loadingService.showLoading();
      this.notificationsMsgService.searchMessages(this.searchText, this.skip, this.take).then((res) => {
        this.messages = (this.messages || []).concat(res.value);
        this.count = res.total;
        (this.messages || []).map((message) => {
          message.dateReceived == null ? message.read = false : message.read = true;
          message.body = this._sanitizer.sanitize(1, message.body);
          return message;
        });
        this.isLoading = false;
        this.loadingService.hideLoading();
      });
    }
  }

  async setMessageAsRead(message) {
    if (message) {
      message.dateReceived = new Date();
      await this.notificationsMsgService.setMessageAsRead(message.id);
      setTimeout(() => {
        message.read = true;
        this.messageSharedService.callUnReadMessages();
       }, 1500);
    }
  }

  onSearchTextKeyDown($event: any) {
    if ($event.keyCode === 13) {
      this.selectedMessages = 'all';
      this.getMessages(this.selectedMessages, 0, 0);
    }
  }

  onSearchTextKeyUp($event: any) {
    if ($event.target && $event.target.value.length === 0) {
      this.selectedMessages = 'all';
      this.getMessages( this.selectedMessages, 0, 0 );
    }
  }

  onScroll() {
    if (this.count > (this.skip + this.take)) {
      this.skip += this.take;
      this.getMessages(this.selectedMessages, this.skip, this.messages.length);
    }
  }

  // download the document file
  downloadFile(attachment) {
    this.notificationsMsgService.downloadFile(attachment);
  }

  onChange($event: Event) {
    this._router.navigate(['messages/list', this.selectedMessages]);
  }

  showMore(message) {
    message = Object.assign(message, {bodyFull : message.body});
  }

  showLess(message) {
    delete message.bodyFull;
  }
}
