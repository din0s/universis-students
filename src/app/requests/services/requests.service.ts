import { Injectable, OnInit } from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import { ConfigurationService } from '@universis/common';
import { RequestTypesService, RequestTypeItem } from '@universis/common';
import {TranslateService} from '@ngx-translate/core';

@Injectable()
export class RequestsService {

  private isInitialized = false;

  constructor(
    private _context: AngularDataContext,
    private _configuration: ConfigurationService,
    private _requestTypesService: RequestTypesService,
    private _translateService: TranslateService
    ) { }

  /**
   *
   * Retrieves the available request types
   *
   */
  async getRequestTypes(): Promise<Array<RequestTypeItem>>  {

    if (!this.isInitialized) {
      await this.populateRequestTypes();
      this.isInitialized = true;
    }

    return this._requestTypesService.getItems();
  }

  /**
   *
   * Reads the document types
   *
   */
  getDocumentTypes() {
    return this._context.model('DocumentConfigurations')
    .asQueryable()
    .where('inLanguage')
    .equal(this._configuration.currentLocale)
    .getItems();

  }

  /**
   *
   * Adds one or more items at the request types service.
   *
   * @param {Array<RequestTypeItem>} items The list of the request types items
   *
   */
  addRequestTypes(...items: Array<RequestTypeItem>) {
    this._requestTypesService.addRange(...items);
  }

  getActiveDocumentRequests() {
    return this._context.model('RequestDocumentActions')
      .asQueryable()
      .where('actionStatus/alternateName')
      .equal('ActiveActionStatus')
      .expand('object', 'actionStatus, messages($orderby=dateCreated desc;$expand=attachments)')
      .orderByDescending('dateCreated')
      .take (-1)
      .getItems();
  }


  getDocumentRequests() {
    return this._context.model('RequestDocumentActions')
      .asQueryable()
      .expand('object', 'actionStatus, messages($orderby=dateCreated desc;$expand=attachments)')
      .orderByDescending('dateCreated')
      .take (-1)
      .getItems();
  }

  getMessageRequests() {
    return this._context.model('RequestMessageActions')
      .asQueryable()
      .expand('actionStatus, messages($orderby=dateCreated desc;$expand=attachments)')
      .orderByDescending('dateCreated')
      .take (-1)
      .getItems();
  }

  getDocumentRequest(id) {
    return this._context.model('RequestDocumentActions')
      .asQueryable()
      .where('id')
      .equal(id)
      .expand('object', 'actionStatus, messages($orderby=dateCreated desc;$expand=attachments)')
      .getItem();
  }



  downloadFile(attachments) {
    const headers = new Headers();
    const serviceHeaders = this._context.getService().getHeaders();
    Object.keys(serviceHeaders).forEach((key) => {
      if (serviceHeaders.hasOwnProperty(key)) {
        headers.set(key, serviceHeaders[key]);
      }
    });
    const attachURL = attachments[0].url.replace(/\\/g, '/').replace('/api', '');

    const fileURL = this._context.getService().resolve(attachURL);
    fetch(fileURL, {
      headers: headers,
      credentials: 'include'
    }).then((response) => {

      return response.blob();
    })
      .then(blob => {

        const objectUrl = window.URL.createObjectURL(blob);
        const a = document.createElement('a');
        document.body.appendChild(a);
        a.setAttribute('style', 'display: none');
        a.href = objectUrl;
        a.download = `${attachments[0].name}`;
        a.click();
        window.URL.revokeObjectURL(objectUrl);
        a.remove();
      });
  }

    /**
   *
   * Add the document types and the default request
   *
   */
  async populateRequestTypes() {

    const documentTypes = await this.getDocumentTypes();
    const documentRequestTypeItems = documentTypes.map((item) => {
      const requestTypeItem: RequestTypeItem = {
        name: item.name,
        alternateName: item.alternateName,
        category: 'Requests.Certificates',
        entryPoint: 'requests/new'
      };

      return requestTypeItem;
    });
    this.addRequestTypes(...documentRequestTypeItems);

    const studentRequestTypes = {
      name: this._translateService.instant('NewRequestTemplates.OtherRequest.Name'),
      alternateName: 'OtherRequest',
      category: 'Requests.Requests',
      entryPoint: `requests/new`
    };
    this.addRequestTypes(studentRequestTypes);
  }
}
