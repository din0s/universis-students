import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {CurrentRegistrationService, REGISTRATION_STATUS} from '../../services/currentRegistrationService.service';
import {ProfileService} from '../../../profile/services/profile.service';
import {TranslateService} from '@ngx-translate/core';
import { LoadingService } from '@universis/common';
import { LocalizedDatePipe } from '@universis/common';
import {ErrorService} from '@universis/common';

@Component({
  selector: 'app-registrations-semester',
  templateUrl: 'registrations-semester.component.html'
})
export class RegistrationSemesterComponent implements OnInit {

  params: any = {};
  message: any;
  action: any;
  disableButton: boolean;
  private registrationSemesterPeriodStart: Date;
  private registrationSemesterPeriodEnd: Date;
  public start: string;
  public end: string;
  public Status: any;


  constructor(private _currentRegistrationService: CurrentRegistrationService,
              private _translateService: TranslateService,
              private _profileService: ProfileService,
              private _router: Router,
              private _loading: LoadingService,
              private _errorService: ErrorService) { }

  async ngOnInit() {
    this._loading.showLoading();
    try {
      const student = await this._profileService.getStudent();
      if (student &&
        student.department &&
        student.department.organization &&
        student.department.organization.instituteConfiguration &&
        !student.department.organization.instituteConfiguration.useStudentRegisterAction) {
        return this._router.navigate(['/registrations/courses']);
      }
      // get register action for current academic period
      // note: student institute uses student register actions
      // so first of all we should check if student has been registered
      const registerAction = await this._currentRegistrationService.getCurrentRegisterAction();
      // get registration period start and end formatter
      const formatter = new LocalizedDatePipe(this._translateService);
      // get registration period info for current academic period
      const registrationPeriodInfo = student.department.organization.registrationPeriods.find( x => {
        return x.academicPeriod === student.department.currentPeriod.id &&
          x.academicYear === student.department.currentYear.id;
      });
      let statusText;
      let validPeriod = false;

      // if registration period has been set yet (and register action is null)
      if (registerAction == null) {
        if (registrationPeriodInfo == null) {
          // registration period is not available at all
          statusText = REGISTRATION_STATUS.toString(REGISTRATION_STATUS.NotAvailable);
          // set messages
          this.message = this._translateService.instant(`SemesterRegistration.${statusText}`, this.params);
          this.Status = statusText;
          // hide loading
          this._loading.hideLoading();
          // and return
          return;
        }
        // check registration period info dates
        const currentDate = new Date();
        validPeriod = registrationPeriodInfo.registrationPeriodStart <= currentDate &&
          currentDate <= registrationPeriodInfo.registrationPeriodEnd;
        if (!validPeriod) {
          // check if semester registration period is passed
          if ( currentDate > registrationPeriodInfo.registrationPeriodEnd) {
            statusText = REGISTRATION_STATUS.toString(REGISTRATION_STATUS.Deadline);
          } else if ( currentDate < registrationPeriodInfo.registrationPeriodStart) {
            // check if registration period is coming
            statusText = REGISTRATION_STATUS.toString(REGISTRATION_STATUS.NotAvailableTill);
          }
        }
      } else {
          // registration period is not available at all
          statusText = REGISTRATION_STATUS.toString(REGISTRATION_STATUS.Successful);
      }
      if (registrationPeriodInfo == null) {
        this.params = Object.assign( student.department);
      } else {
        this.registrationSemesterPeriodStart =  registrationPeriodInfo.registrationPeriodStart;
        this.registrationSemesterPeriodEnd = registrationPeriodInfo.registrationPeriodEnd;
        if (this.registrationSemesterPeriodStart && this.registrationSemesterPeriodEnd) {
          this.start = (this.registrationSemesterPeriodStart.getDate().toString() + '/' +
            + (this.registrationSemesterPeriodStart.getMonth() + 1).toString() + '/' +
            + this.registrationSemesterPeriodStart.getFullYear());

          this.end = (this.registrationSemesterPeriodEnd.getDate().toString() + '/' +
            + (this.registrationSemesterPeriodEnd.getMonth() + 1).toString() + '/' +
            + this.registrationSemesterPeriodEnd.getFullYear());
        } else {
          this.start = this.end = null;
        }
        student.department.currentPeriod.name = this._translateService.instant(student.department.currentPeriod.alternateName);
      this.params = Object.assign({
        'registrationPeriodStartDate': formatter.transform(registrationPeriodInfo.registrationPeriodStart, 'mediumDate'),
        'registrationPeriodEndDate': formatter.transform(registrationPeriodInfo.registrationPeriodEnd, 'mediumDate')
      }, student.department); }
      // get messages according to current registration status
      if ( !statusText && student.studentStatus ) {
        if (student.studentStatus.alternateName !== 'active') {
          statusText = REGISTRATION_STATUS.toString(REGISTRATION_STATUS.NotActiveStudent);
        } else if (student.studyProgram.studyLevel === 2 && registerAction == null) {
          statusText = REGISTRATION_STATUS.toString(REGISTRATION_STATUS.Postgraduate);
        } else if (registerAction == null && student.studentStatus.alternateName === 'active' && student.studyProgram.studyLevel === 1) {
          statusText = REGISTRATION_STATUS.toString(REGISTRATION_STATUS.Available);
        } else {
          statusText = REGISTRATION_STATUS.toString(REGISTRATION_STATUS.Successful);
        }
      }

      this.Status = statusText;
      this.message = {
        'title': this._translateService.instant('Caps.' + student.department.currentPeriod.alternateName) +
        ' ' + this._translateService.instant('Caps.semester') +
        ' ' + student.department.currentYear.name,
        'icon': `SemesterRegistration.${statusText}.icon`,
        'info': `SemesterRegistration.${statusText}.info`,
        'message': `SemesterRegistration.${statusText}.message`,
        'extraMessage': `SemesterRegistration.${statusText}.extraMessage`,
        'actionButton': `SemesterRegistration.${statusText}.actionButton`,
        'actionText': `SemesterRegistration.${statusText}.actionText`,
        'action': `SemesterRegistration.${statusText}.action`
      };
      // Get Action for button
      this.action = this._translateService.instant(this.message.action);
      this._loading.hideLoading();
    } catch  (err) {
      this._loading.hideLoading();
      // noinspection JSIgnoredPromiseFromCall
      this._errorService.navigateToError(err);
    }
  }
  registerSemester() {
    this._currentRegistrationService.registerSemester().then(() => {
      this.disableButton = true;
      window.location.href = ('/#/registrations/semester');
      window.location.reload(true);
    }).catch( err => {
      // noinspection JSIgnoredPromiseFromCall
      this._errorService.navigateToError(err);
    });
  }
 }
