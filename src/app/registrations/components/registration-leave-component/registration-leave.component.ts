import { Component, OnInit } from '@angular/core';
import {Subject} from 'rxjs';
import {BsModalRef} from 'ngx-bootstrap';
import {CurrentRegistrationService} from '../../services/currentRegistrationService.service';


@Component({
  selector: 'app-registration-leave',
  templateUrl: './registration-leave.component.html'
})
export class RegistrationLeaveComponent implements OnInit {
  subject: Subject<boolean>;

  constructor(public bsModalRef: BsModalRef, private _currentRegistrationService: CurrentRegistrationService) {
  }

  action(value: boolean, action?: string) {
    this.bsModalRef.hide();
    if (value === true) {
      if (action === 'reset') {
        this._currentRegistrationService.reset();
      } else {
        // set registrationEdited value
        sessionStorage['registrationEdited'] = true;
      }
    }
    this.subject.next(value);
    this.subject.complete();
  }
  ngOnInit(): void {
  }
}

