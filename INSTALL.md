# UniverSIS-students Installation

## Prerequisites

Node.js version >6.14.3 is required. Visit [Node.js](https://nodejs.org/en/)
and follow the installation instructions provided for your operating system.

## Install dependencies

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.8. To **install angular-cli** execute

`npm install -g @angular/cli@6`

You may need to run it as root.


Navigate to application directory and execute:

`npm i`

## Deploy
### Development

1. Run `ng serve` for a devevelopment server
- Navigate to `http://localhost:7001/`

The app will automatically reload if you change any of the source files.

### Production
1. Configuration

  `cp src/assets/config/app.json src/assets/config/app.production.json`

  Add the appropriate values for client_id, client_secret and URLs in `src/assets/config/app.production.json`

- Make sure you build from the source code after every change

    `npm run build`

- Use pm2 to manage the application
  * start

    `pm2 start pm2.config.json`

    Navigate to `http://localhost:7001/`

  * check the status

    `pm2 status universis_students`

  * stop

    `pm2 stop universis_students`
    
### Testing
We use Jasmine/Karma to write and run tests for all our projects. Tests are executed on ChromeHeadless using puppeteer. Puppeteer is listed as a peerDependency and must be installed in the following way:
1. Navigate to the directory with (all) your project(s) which is typically done with `cd ..` The tree should look something like:  
<pre>
├── universis
│   └── node_modules
├── universis-students
│   └── node_modules
└── universis-teachers
    └── node_modules
</pre>
2. Run `npm install puppeteer`. Your tree should now look like:
<pre>
├── universis
│   └── node_modules
├── universis-students
│   └── node_modules
├── universis-teachers
│   └── node_modules
└── node_modules
    └── puppeteer
</pre>
3. You should now be able to navigate to a project directory such as `cd universis-students` and run tests with either of two options:
-
  - `npm run test` will launch Karma in a Chromium browser, will watch for your changes and run tests on save. To kill this you have to ^C on terminal, or close Chromium from the UI **three times**. Use this for **development**.
  - `npm run test-ci` will launch Karma in a headless browser, will **NOT** watch for changes and will exit on completion. Use this for **CI**.
