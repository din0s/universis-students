import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanDeactivate, RouterStateSnapshot} from '@angular/router';
import {Observable, Subject} from 'rxjs';
import {RegistrationCoursesComponent} from '../components/registrations-courses/registrations-courses.component';
import {RegistrationLeaveComponent} from '../components/registration-leave-component/registration-leave.component';
import {BsModalService, ModalOptions} from 'ngx-bootstrap';

@Injectable()
export class RegistrationCoursesCanDeactivateGuard implements CanDeactivate<RegistrationCoursesComponent> {

  constructor(private modal: BsModalService) {
  }

  canDeactivate(
    component: RegistrationCoursesComponent,
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
    nextState?: RouterStateSnapshot
  ): Observable<boolean> | boolean {

    if (!component.registrationEdited) {
      return true;
    }
    const subject = new Subject<boolean>();
    if (nextState && nextState.url.indexOf('registrations/courses/checkout') > 0) {
      return true;
    }
    const config: ModalOptions = {
      animated: true,
      class: 'modal-dialog-centered modal-lg border-0',
      ignoreBackdropClick: true,
      keyboard : false
    };
    const modalRef = this.modal.show(RegistrationLeaveComponent, config);
    modalRef.content.subject = subject;
    return subject.asObservable();
  }
}
