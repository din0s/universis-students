import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {MostModule} from '@themost/angular';
import { ConfigurationService } from '@universis/common';
import {TranslateModule} from '@ngx-translate/core';
import {RouterTestingModule} from '@angular/router/testing';
import {FormsModule} from '@angular/forms';
import {AngularDataContext} from '@themost/angular';
import {Router} from '@angular/router';
import {SharedModule} from '@universis/common';
import {BsModalService, ComponentLoaderFactory, ModalModule, PositioningService} from 'ngx-bootstrap';
import { ModalService } from '@universis/common';

import {RequestsNewComponent} from './requests-new.component';
import {TestingConfigurationService} from '../../../test';


describe('RequestsNewComponent', () => {
    let component: RequestsNewComponent;
    let fixture: ComponentFixture<RequestsNewComponent>;

    beforeEach(async(() => {
        return TestBed.configureTestingModule({
            declarations: [
                RequestsNewComponent
            ],
            imports: [
                HttpClientTestingModule,
                TranslateModule.forRoot(),
                RouterTestingModule,
                FormsModule,
                MostModule.forRoot({
                    base: '/',
                    options: {
                        useMediaTypeExtensions: false
                    }
                }),
                SharedModule,
                ModalModule
            ],
            providers: [
                {
                    provide: ConfigurationService,
                    useClass: TestingConfigurationService
                },
                BsModalService,
                ModalService,
                ComponentLoaderFactory,
                PositioningService
            ],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(RequestsNewComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should have loading property equal false by default', () => {
      expect(component.isLoading).toEqual(false);
    });

    it('should have an error state equal to false by default', () => {
      expect(component.hasError).toEqual(false);
    })

    describe('Given that the submit button was pressed', () => {
      let contextMock: AngularDataContext;
      let routerMock: Router;
      let routerSpy: jasmine.Spy;

      beforeEach(() => {
        contextMock = fixture.debugElement.injector.get(AngularDataContext);
        routerMock = fixture.debugElement.injector.get(Router);

        routerSpy = spyOn(routerMock, 'navigate');
      });

      describe('and the submission was successful', () => {
        beforeEach(() => {
          spyOn(contextMock, 'model').and.returnValue({
            save: () =>  Promise.resolve()
          });
        });

        it('should set to loading state after calling the submit function', async() => {
          await component.submit();
          expect(component.isLoading).toEqual(true);
        });

        it('should navigate back to list', async() => {
          await component.submit();
          expect(routerSpy).toHaveBeenCalled();
          expect(routerSpy).toHaveBeenCalledWith(['/requests/list']);
        });
      });


      describe('and there is an error', () => {
        beforeEach(() => {
          spyOn(contextMock, 'model').and.returnValue({
            save: () =>  Promise.reject('kaboom!')
          });
        });

        it('should reset loading state on errors', async() => {
          await component.submit()
          expect(component.isLoading).toEqual(false);
        });

        it('should set hasError to true, when there is an error', async() => {
          await component.submit();
          expect(component.hasError).toEqual(true);
        });

        it('should not change the location', async() => {
          await component.submit();
          expect(routerSpy).not.toHaveBeenCalled();
        });
      });
    });
  });

