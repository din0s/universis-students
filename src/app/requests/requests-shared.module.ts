import {ModuleWithProviders, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MostModule} from '@themost/angular';
import {RequestsService} from './services/requests.service';
import {ProfileSharedModule} from '../profile/profile-shared.module';
import {AutoRequestComponent} from './components/auto-request/auto-request.component';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {environment} from '../../environments/environment';
import {TooltipModule} from 'ngx-bootstrap';


@NgModule({
  imports: [
    CommonModule,
    MostModule,
    TranslateModule,
    ProfileSharedModule,
    TooltipModule
  ],
  declarations: [
    AutoRequestComponent
  ],
  exports: [
    AutoRequestComponent
  ],
})

export class RequestsSharedModule {
  constructor(private _translateService: TranslateService) {
    environment.languages.forEach((culture) => {
      import(`./i18n/requests.${culture}.json`).then((translations) => {
        this._translateService.setTranslation(culture, translations, true);
      });
    });
  }
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: RequestsSharedModule,
      providers: [
        RequestsService
        ]
    };
  }
}
